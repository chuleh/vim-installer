#!/bin/bash

# update and install vim and vim-gui
apt-get update && apt-get install -y curl vim-gtk vim

# download vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# download molokai colorscheme
curl -fLo ~/.vim/colors/molokai.vim --create-dirs \
    https://gitlab.com/chuleh/vimrc/raw/master/molokai.vim

# download my vim config file and move it to $HOME
curl -fLo https://gitlab.com/chuleh/vimrc/blob/master/.vimrc && mv .vimrc ~/.vimrc

# install plugins and quit
vim +PlugInstall +qall
